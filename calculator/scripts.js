$(document).ready(function() {
  var keys  = $('#calculator span.number');
  var pi    = $('#calculator span.pi');
  var sin   = $('#calculator span.sin');

  var input    = $('.screen');
  var setClear = false;

  for(var i = 0; i < keys.length; i++){
    $(keys[i]).click(function(e) {

      if (setClear) {
        input.html('');
      }

      // Button value
      var btnVal = $(this).text();

      // Add input to screen
      input.append(btnVal);
      setClear = false;

      // Clear Screen
      if(btnVal == 'C') {
        input.html('');
      }
    });
  }

  sin.click(function(e){
    input.html(Math.sin(input.text()));
    setClear = true;
  });

  pi.click(function(e){
    input.html(Math.PI);
    setClear = true;
  });
});
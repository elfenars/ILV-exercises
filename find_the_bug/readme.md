The problem was the scope of the variable ``value``.

Inside the ```show``` function ```this.value``` existed as the previous value passed from ``init``,
but inside the ``setTimeout`` function that value was being referenced as ``this.value`` also and the ``setTimeout`` didn't even know that it existed.

The solution is to reassing this.value in the ``show`` function to a bigger scope (for example: another variable called ``value``)
and then reassign it as ``this.value`` inside the ``setTimeout`` function.

*** THIS IS ONLY ONE WAY TO GO, THERE ARE PLENTY ***